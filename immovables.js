const kvartiry = {
  title: 'Квартиры',
  name: 'immovables-kvartiry',
  attributes: [
    {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
    {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
    {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
    {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    {"label":"Рынок","name":"rynok","type":"enum","required":false,"index":2,"filter":true,"options":["Первичный","Вторичный"]},
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Продам","Сдам","Куплю","Сниму","Обмен"]},
    {"label":"Метро","name":"metro","type":"enum","required":false,"index":4,"filter":true,"options":["Холодная гора","Южный вокзал","Центральный рынок","Советская - пересадка на станцию Исторический музей","Проспект Гагарина","Спортивная – пересадка на станцию Метростроителей","Завод имени Малышева","Московский проспект","Маршала Жукова","Советской Армии","Имени Масельского","Тракторный завод","Пролетарская","Исторический музей","Университет – пересадка на станцию Госпром","Пушкинская","Киевская","Академика Барабашова","Академика Павлова","Студенческая","Героев Труда","Метростроителей","Площадь Восстания","Архитектора Бекетова","Госпром – пересадка на станцию Университет","Научная","Ботанический сад","23 Августа","Алексеевская"]},
    {"label":"Район","name":"rayon","type":"enum","required":false,"index":3,"filter":true,"options":["киевский","московский","индустриальный","немышлянский","новобаварский","основянский","слободской","холодногорский","шевченковский"]},

  ],
  children: [
  ],
};

const komnaty = {
  title: 'Комнаты',
  name: 'immovables-komnaty',
  attributes: [
    {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
    {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
    {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
    {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Продам","Сдам","Куплю","Сниму","Обмен"]},
    {"label":"Рынок","name":"rynok","type":"enum","required":false,"index":2,"filter":true,"options":["Первичный","Вторичный"]},
    {"label":"Метро","name":"metro","type":"enum","required":false,"index":4,"filter":true,"options":["Холодная гора","Южный вокзал","Центральный рынок","Советская - пересадка на станцию Исторический музей","Проспект Гагарина","Спортивная – пересадка на станцию Метростроителей","Завод имени Малышева","Московский проспект","Маршала Жукова","Советской Армии","Имени Масельского","Тракторный завод","Пролетарская","Исторический музей","Университет – пересадка на станцию Госпром","Пушкинская","Киевская","Академика Барабашова","Академика Павлова","Студенческая","Героев Труда","Метростроителей","Площадь Восстания","Архитектора Бекетова","Госпром – пересадка на станцию Университет","Научная","Ботанический сад","23 Августа","Алексеевская"]},
    {"label":"Район","name":"rayon","type":"enum","required":false,"index":3,"filter":true,"options":["киевский","московский","индустриальный","немышлянский","новобаварский","основянский","слободской","холодногорский","шевченковский"]},

  ],
  children: [
  ],
};

const gostinkimalogabaritki = {
  title: 'Гостинки/малогабаритки',
  name: 'immovables-gostinkimalogabaritki',
  attributes: [
    {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
    {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
    {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Продам","Сдам","Куплю","Сниму","Обмен"]},
    {"label":"Рынок","name":"rynok","type":"enum","required":false,"index":2,"filter":true,"options":["Первичный","Вторичный"]},
    {"label":"Метро","name":"metro","type":"enum","required":false,"index":4,"filter":true,"options":["Холодная гора","Южный вокзал","Центральный рынок","Советская - пересадка на станцию Исторический музей","Проспект Гагарина","Спортивная – пересадка на станцию Метростроителей","Завод имени Малышева","Московский проспект","Маршала Жукова","Советской Армии","Имени Масельского","Тракторный завод","Пролетарская","Исторический музей","Университет – пересадка на станцию Госпром","Пушкинская","Киевская","Академика Барабашова","Академика Павлова","Студенческая","Героев Труда","Метростроителей","Площадь Восстания","Архитектора Бекетова","Госпром – пересадка на станцию Университет","Научная","Ботанический сад","23 Августа","Алексеевская"]},
    {"label":"Район","name":"rayon","type":"enum","required":false,"index":3,"filter":true,"options":["киевский","московский","индустриальный","немышлянский","новобаварский","основянский","слободской","холодногорский","шевченковский"]},

  ],
  children: [
  ],
};

const domadachiuchastki = {
  title: 'Дома/дачи/участки',
  name: 'immovables-domadachiuchastki',
  attributes: [
    {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Продам","Сдам","Куплю","Сниму","Обмен"]},
    {"label":"Метро","name":"metro","type":"enum","required":false,"index":4,"filter":true,"options":["Холодная гора","Южный вокзал","Центральный рынок","Советская - пересадка на станцию Исторический музей","Проспект Гагарина","Спортивная – пересадка на станцию Метростроителей","Завод имени Малышева","Московский проспект","Маршала Жукова","Советской Армии","Имени Масельского","Тракторный завод","Пролетарская","Исторический музей","Университет – пересадка на станцию Госпром","Пушкинская","Киевская","Академика Барабашова","Академика Павлова","Студенческая","Героев Труда","Метростроителей","Площадь Восстания","Архитектора Бекетова","Госпром – пересадка на станцию Университет","Научная","Ботанический сад","23 Августа","Алексеевская"]},
    {"label":"Район","name":"rayon","type":"enum","required":false,"index":3,"filter":true,"options":["киевский","московский","индустриальный","немышлянский","новобаварский","основянский","слободской","холодногорский","шевченковский"]},

  ],
  children: [
    {"name":"immovables-domadachiuchastki-dom","title":"Дом", attributes: [
        {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    ],},
    {"name":"immovables-domadachiuchastki-chast","title":"Часть дома", attributes: [
        {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    ],},
    {"name":"immovables-domadachiuchastki-dacha","title":"Дача", attributes: [
        {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    ],},
    {"name":"immovables-domadachiuchastki-nedostroy","title":"Недострой", attributes: [
        {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    ],},
    {"name":"immovables-domadachiuchastki-zem","title":"Зем. участок", attributes: [
        {"label":"категория земель","name":"kategoriya","type":"enum","required":false,"index":11,"filter":true,"options":["Земля сельскохозяйственного назначения","Земля жилой и общественной застройки","Земля промышленности, транспорта и др. назначения","Другое"]},

      ]},
  ],
};

const garazhi = {
  title: 'гаражи',
  name: 'immovables-garazhi',
  attributes: [
    {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Продам","Сдам","Куплю","Сниму","Обмен"]},
    {"label":"Метро","name":"metro","type":"enum","required":false,"index":4,"filter":true,"options":["Холодная гора","Южный вокзал","Центральный рынок","Советская - пересадка на станцию Исторический музей","Проспект Гагарина","Спортивная – пересадка на станцию Метростроителей","Завод имени Малышева","Московский проспект","Маршала Жукова","Советской Армии","Имени Масельского","Тракторный завод","Пролетарская","Исторический музей","Университет – пересадка на станцию Госпром","Пушкинская","Киевская","Академика Барабашова","Академика Павлова","Студенческая","Героев Труда","Метростроителей","Площадь Восстания","Архитектора Бекетова","Госпром – пересадка на станцию Университет","Научная","Ботанический сад","23 Августа","Алексеевская"]},
    {"label":"Район","name":"rayon","type":"enum","required":false,"index":3,"filter":true,"options":["киевский","московский","индустриальный","немышлянский","новобаварский","основянский","слободской","холодногорский","шевченковский"]},

  ],
  children: [
    {"name":"immovables-garazhi-metalicheskiy","title":"металический"},
    {"name":"immovables-garazhi-zhelezobetonnyy","title":"Железобетонный"},
    {"name":"immovables-garazhi-garazh","title":"гараж в кооперативе"},
    {"name":"immovables-garazhi-mashinomesto","title":"Машиноместо"},
  ],
};

const komercheskaya = {
  title: 'комерческая недвижимость',
  name: 'immovables-komercheskaya',
  attributes: [
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Продам","Сдам","Куплю","Сниму","Обмен"]},
    {"label":"Метро","name":"metro","type":"enum","required":false,"index":4,"filter":true,"options":["Холодная гора","Южный вокзал","Центральный рынок","Советская - пересадка на станцию Исторический музей","Проспект Гагарина","Спортивная – пересадка на станцию Метростроителей","Завод имени Малышева","Московский проспект","Маршала Жукова","Советской Армии","Имени Масельского","Тракторный завод","Пролетарская","Исторический музей","Университет – пересадка на станцию Госпром","Пушкинская","Киевская","Академика Барабашова","Академика Павлова","Студенческая","Героев Труда","Метростроителей","Площадь Восстания","Архитектора Бекетова","Госпром – пересадка на станцию Университет","Научная","Ботанический сад","23 Августа","Алексеевская"]},
    {"label":"Район","name":"rayon","type":"enum","required":false,"index":3,"filter":true,"options":["киевский","московский","индустриальный","немышлянский","новобаварский","основянский","слободской","холодногорский","шевченковский"]},

  ],
  children: [
    {"name":"immovables-komercheskaya-gostinica","title":"Гостиница", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-ofisnoe","title":"Офисное помещение", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-eda","title":"Помещение общественного питания", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-free","title":"Помещение свободного назначения", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-proizvodstvennoe","title":"Производственное помещение", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-skladskoe","title":"Складское помещение", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-uchastki","title":"участки под бизнес", attributes: [
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
        {"label":"категория земель","name":"kategoriya","type":"enum","required":false,"index":11,"filter":true,"options":["Земля сельскохозяйственного назначения","Земля жилой и общественной застройки","Земля промышленности, транспорта и др. назначения","Другое"]},

      ],},
    {"name":"immovables-komercheskaya-mesta","title":"места на рынках"},
    {"name":"immovables-komercheskaya-kioski","title":"киоски, павильоны", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-torgovoe","title":"Торговое помещение", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},
    {"name":"immovables-komercheskaya-drugaya","title":"Другая", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
    ],},

  ],
};

const nedvizhimost = {
  title: 'недвижимость за рубежом',
  name: 'immovables-nedvizhimost',
  attributes: [
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Продам","Сдам"]},
  ],

  children: [
    {"name":"immovables-nedvizhimost-kvartiry","title":"квартиры/ аппартаменты", attributes: [
        {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этаж","name":"etazh","type":"value","required":false,"index":8,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
    ],},
    {"name":"immovables-nedvizhimost-domavillykotedzhi","title":"дома/виллы/котеджи", attributes: [
        {"label":"Количество комнат","name":"kolichestvo","type":"value","required":false,"index":6,"filter":true},
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},
    {"name":"immovables-nedvizhimost-zemelnye","title":"земельные участки", attributes: [
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
        {"label":"категория земель","name":"kategoriya","type":"enum","required":false,"index":11,"filter":true,"options":["Земля сельскохозяйственного назначения","Земля жилой и общественной застройки","Земля промышленности, транспорта и др. назначения","Другое"]},

      ],},
    {"name":"immovables-nedvizhimost-kommercheskaya","title":"коммерческая недвижимость", attributes: [
        {"label":"Общая площадь","name":"obshchaya","type":"value","required":false,"index":7,"filter":true},
        {"label":"Этажность дома","name":"etazhnost","type":"value","required":false,"index":9,"filter":true},
        {"label":"Площадь участка","name":"ploshchad","type":"value","required":false,"index":10,"filter":true},
    ],},

  ],
};

const arenda = {
  title: 'Аренда посуточно/почасово',
  name: 'immovables-arenda',
  attributes: [
    {"label":"Тип объявления","name":"tip","type":"enum","required":false,"index":1,"filter":true,"options":["Сдам","Сниму"]},
  ],

  children: [
    {name: 'immovables-arenda-kvartiry', title: 'Квартиры'},
    {name: 'immovables-arenda-komnaty', title: 'Комнаты'},
    {name: 'immovables-arenda-gostinki', title: 'Гостинки'},
    {name: 'immovables-arenda-domadachi', title: 'Дома/Дачи'},
  ]
};

module.exports = {
  title: 'Недвижимость',
  name: 'immovables',
  attributes: [
    {"label":"Цена","name":"price","type":"radio","required":true,"index":5,"filter":true,"options":[{"label":"Цена","name":"price","type":"value","index":1},{"label":"Договорная","name":"dogovor","type":"bool","index":2},{"label":"Бесплатно","name":"besplatno","type":"bool","index":3}]},
  ],

  children: [
    kvartiry,
    komnaty,
    gostinkimalogabaritki,
    domadachiuchastki,
    arenda,
    garazhi,
    komercheskaya,
    nedvizhimost,
  ],
};