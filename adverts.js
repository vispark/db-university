const random = require('lodash/random');
const categories = require('./immovables.js');
const MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017';

const dbName = 'myproject';
const collectionName = 'test';

const COLLECTION_TOTAL = 100;


function form({user, category, index}) {
  const types = ['buy', 'sell', 'rent', 'for rent', 'other'];
  const value = name => {
    switch (name) {

      case 'Цена': {
        return random(1000, 100000);
      }

      default: {
        return random(70);
      }
    }
  };

  const attributes = category.attributes.map(attribute => {
    switch (attribute.type) {

      case 'bool': {
        return {
          name: attribute.name,
          value: random(1),
        };
      }

      case 'enum': {
        return {
          name: attribute.name,
          value: attribute.options[random(attribute.options.length -1)],
        };
      }

      // TODO: multiple

      case 'value': {
        return {
          name: attribute.name,
          value: value(attribute.name),
        };
      }

      default: {
        break;
      }
    }
  });

  return {
    type: types[random(types.length -1)],
    title: `Заголовок к объявлению номер № ${index}`,
    plain_body: `
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis risus nulla, sodales sit amet commodo pharetra,
      hendrerit quis purus. Maecenas feugiat, magna eu blandit euismod, arcu augue venenatis dui,
      at pulvinar erat elit quis tortor. Donec feugiat, felis at pellentesque hendrerit, lorem turpis maximus dolor,
      vitae pharetra nunc ante eget libero. Nunc tincidunt orci ante, at vulputate tortor imperdiet vel.
      Aliquam sit amet tortor in est eleifend vulputate. Nunc sit amet ornare libero. Sed vel lacinia sapien.
      Vestibulum lacinia ante id leo aliquam, vel accumsan dolor aliquam. Quisque at odio in nisi varius aliquet nec vitae tellus.
      Vestibulum at tristique lacus, quis mattis massa.
      Aliquam non porttitor ipsum. Curabitur ultricies ex vel velit ultrices mollis.
      Donec sapien libero, lobortis eget maximus quis, faucibus sed arcu. Vivamus hendrerit vehicula purus ac semper.
      Cras vulputate urna felis, nec facilisis enim tempor pulvinar. Fusce pellentesque est tortor, non volutpat ex eleifend in.
      Nunc erat felis, pellentesque eget hendrerit eget, congue ac neque. Fusce nec nunc at nulla rhoncus volutpat quis at mauris.
      Curabitur porttitor finibus lectus. Aliquam sed interdum tellus.
    `,
    publisher: user,
    attributes,
    categories: 'Недвижимость - Квартиры',
    medias: [], // TODO: medias
  };
}

function adverts() {

  try {

    MongoClient.connect(url, async (err, client) => {
      console.log("Connected successfully to server");

      const db = client.db(dbName);


      for (let index = 0; index <= COLLECTION_TOTAL; index++) {
        const data = form({
          user: 'user_test',
          category: categories.children[random(categories.children.length -1)],
          index,
        });

        await db.collection(collectionName).insert(data);
      }

      client.close();
    });


  } catch (error) {
    console.log(error.message);
  }
}

adverts();